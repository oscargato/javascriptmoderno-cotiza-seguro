//Constructor para Seguros
function Seguro(marca,anio,tipo){
	this.marca = marca;
	this.anio = anio;
	this.tipo = tipo;
}

//Seguro cotizar
Seguro.prototype.cotizarSeguro = function(informacion){
	let cantidad
	const base = 2000;
	switch(this.marca){
		case '1': cantidad = base * 1.15;
				  break;
		case '2': cantidad = base * 1.05;
				  break;
		case '3': cantidad = base * 1.35;
				  break;		  
	}

	//Leer anio
	const diferencia = new Date().getFullYear() - this.anio;

	//Cada anio de diferencia hay que reducir 3% el valor del seguro
	cantidad -= ((diferencia * 3) * cantidad) / 100;


	//Seguro Basico y Completo
	if(this.tipo === 'Basico'){
		cantidad *= 1.30;
	}else{
		cantidad *= 1.50;
	}

	return cantidad;
}

//Todo lo que se muestra
function Interfaz(){

}

//Mensaje de Error
Interfaz.prototype.mostrarMensaje = function(mensaje, tipo){
	const div = document.createElement('div');
	if(tipo === 'error'){
		div.classList.add('mensaje','error');
	}else{
		div.classList.add('mensaje','correcto');
	}
	div.innerHTML = `${mensaje}`;
	formulario.insertBefore(div,document.querySelector('.form-group'));

	setTimeout(function(){
		document.querySelector('.mensaje').remove();
	},3000);
}

//Imprimir el resultado de la cotizacion
Interfaz.prototype.mostrarResultado = function(seguro,total){
	const resultado = document.getElementById('resultado');
	let marca;
	switch(seguro.marca){
		case '1': marca = 'Americano';
		          break;
		case '2': marca = 'Asiatico';
		          break;
		case '3': marca = 'Europeo';
		          break;		          		          
	}

	//Crear Div
	const div = document.createElement('div');

	//Insertar Informacion
	div.innerHTML = `
		<p class="header">Tu Resumen:</p>
		<p>Marca: ${marca}</p>
		<p>Ano: ${seguro.anio}</p>
		<p>Tipo: ${seguro.tipo}</p>
	`;

	const spinner = document.querySelector('#cargando img');
	spinner.style.display = 'block';
	setTimeout(function(){
		spinner.style.display = 'none';
		resultado.appendChild(div);
	},3000);
	//resultado.appendChild(div);
}

//EventListener
const formulario = document.getElementById('cotizar-seguro');
formulario.addEventListener('submit',function(e){
	e.preventDefault();

	//Leer marca seleccionada
	const marca = document.getElementById('marca');
	const marcaSeleccionada = marca.options[marca.selectedIndex].value;

	//Leer el anio seleccionado
	const anio = document.getElementById('anio');
	const anioSeleccionado = anio.options[anio.selectedIndex].value;

	//lee el valor del radio button
	const tipo = document.querySelector('input[name="tipo"]:checked').value; 

	//Crear una instancia de Interfaz
	const interfaz = new Interfaz();

	//Validando campos
	if(marcaSeleccionada === '' || anioSeleccionado === '' || tipo === ''){
		interfaz.mostrarMensaje('Faltan Datos en el Formulario','error');
	}else{
		//Limpiando resultado
		const resultados = document.querySelector('#resultado div');
		if(resultados != null){
			resultados.remove();
		}

		//Instanciar seguro y mostrar interfaz
		const seguro = new Seguro(marcaSeleccionada,anioSeleccionado,tipo);

		//Cotizar el Seguro
		const cantidad = seguro.cotizarSeguro(seguro);

		interfaz.mostrarResultado(seguro,cantidad);
		interfaz.mostrarMensaje('Cotizando...','exito');
	}
});


const max = new Date().getFullYear(),
	  min = max - 20;

const selectAnios = document.getElementById('anio');
for(let i = max; i > min; i--){
	let option = document.createElement('option');
	option.value = i;
	option.innerHTML = i;
	selectAnios.appendChild(option);
}